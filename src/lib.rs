mod file_io;
use eframe::egui;
use egui_data::EguiData;
use eventuals::Eventual;

pub trait SaveFile: Sized {
    fn encode(&self) -> anyhow::Result<Vec<u8>>;
    fn decode(data: &[u8]) -> anyhow::Result<Self>;
}

pub struct SaveEditor<T> {
    data: Option<anyhow::Result<T>>,
    file: Option<Eventual<Option<Vec<u8>>>>,
    errors: Vec<String>,
    view_file_browser: bool,
    view_error_log: bool,
    default_filename: String,
}

impl<T> SaveEditor<T>
where
    T: SaveFile + EguiData + 'static,
{
    pub fn new(_cc: &eframe::CreationContext<'_>, default_filename: String) -> Self {
        Self {
            data: None,
            file: None,
            errors: vec![],
            view_file_browser: false,
            view_error_log: false,
            default_filename,
        }
    }

    #[cfg(not(target_arch = "wasm32"))]
    pub fn run(app_name: &str, default_filename: String) -> eframe::Result<()> {
        let native_options = eframe::NativeOptions::default();
        eframe::run_native(
            app_name,
            native_options,
            Box::new(|cc| Box::new(SaveEditor::<T>::new(cc, default_filename))),
        )
    }

    #[cfg(target_arch = "wasm32")]
    pub fn run(_app_name: &str, default_filename: String) -> eframe::Result<()> {
        let web_options = eframe::WebOptions::default();

        wasm_bindgen_futures::spawn_local(async {
            eframe::WebRunner::new()
                .start(
                    "the_canvas_id", // hardcode it
                    web_options,
                    Box::new(|cc| Box::new(SaveEditor::<T>::new(cc, default_filename))),
                )
                .await
                .expect("failed to start eframe");
        });
        Ok(())
    }
}

impl<T> eframe::App for SaveEditor<T>
where
    T: SaveFile + EguiData,
{
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::TopBottomPanel::top("menu bar").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Open").clicked() {
                        self.file = Some(file_io::load_file());
                    }
                    if ui.button("Save").clicked() {
                        if let Some(Ok(data)) = &self.data {
                            match data
                                .encode()
                                .and_then(|data| file_io::save_file(&self.default_filename, &data))
                            {
                                Ok(_) => (),
                                Err(e) => {
                                    self.errors.push(e.to_string());
                                }
                            }
                        }
                    }
                });
                ui.menu_button("View", |ui| {
                    ui.checkbox(&mut self.view_file_browser, "File Browser");
                    ui.checkbox(&mut self.view_error_log, "Error Log");
                });
            });
        });
        if self.view_file_browser {
            egui::SidePanel::left("file list").show(ctx, |ui| {
                ui.vertical(|ui| {
                    ui.label("file 1");
                    ui.label("file 2");
                    ui.label("file 2");
                });
            });
        }
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Hello World!");
            if self.file.is_some() {
                let eventual = self.file.as_ref().unwrap();
                if let Some(data) = eventual.value_immediate() {
                    if let Some(data) = data {
                        self.data = Some(T::decode(&data));
                    }
                    self.file = None;
                } else {
                    ui.spinner();
                }
            }

            if let Some(data) = &mut self.data {
                match data {
                    Ok(data) => {
                        egui::ScrollArea::vertical().show(ui, |ui| {
                            data.ui_inner(ui, "Save File");
                        });
                    }
                    Err(error) => {
                        ui.label("could not load file:");
                        ui.label(error.to_string());
                    }
                }
            } else {
                if ui.button("load file").clicked() {
                    self.file = Some(file_io::load_file());
                }
            }
        });
        if self.view_error_log {
            egui::TopBottomPanel::bottom("error log").show(ctx, |ui| {
                ui.heading("Errors:");
                for error in &self.errors {
                    ui.label(error);
                }
            });
        }
    }
}
