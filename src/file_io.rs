use eventuals::Eventual;

pub fn load_file() -> Eventual<Option<Vec<u8>>> {
    let (mut writer, eventual) = Eventual::new();

    let f2 = async move {
        let future = async {
            let file = rfd::AsyncFileDialog::new()
                .set_directory(".")
                .pick_file()
                .await?;
            let s = file.read().await;
            Some(s)
        };
        writer.write(future.await)
    };

    #[cfg(not(target_arch = "wasm32"))]
    futures::executor::block_on(f2);
    #[cfg(target_arch = "wasm32")]
    wasm_bindgen_futures::spawn_local(f2);

    eventual
}

#[cfg(not(target_arch = "wasm32"))]
pub fn save_file(name_hint: &str, data: &[u8]) -> anyhow::Result<()> {
    let path = rfd::FileDialog::new()
        .set_directory(".")
        .set_file_name(name_hint)
        .save_file();
    match path {
        Some(path) => Ok(std::fs::write(path, data)?),
        None => Ok(()),
    }
}

#[cfg(target_arch = "wasm32")]
pub fn save_file(name_hint: &str, data: &[u8]) -> anyhow::Result<()> {
    /// adjusted dialog from the rfd crate to allow saving
    /// source https://github.com/PolyMeilex/rfd/blob/25d879a854b606c2236b72c7c085ae776d413837/src/backend/wasm.rs#L9C1-L152C1
    use gloo_file::{Blob, ObjectUrl};
    use wasm_bindgen::prelude::Closure;
    use wasm_bindgen::JsCast;
    use web_sys::{HtmlAnchorElement, HtmlButtonElement};
    let blob = Blob::new(data);
    let url = ObjectUrl::from(blob);

    let window = web_sys::window().expect("Window not found");
    let document = window.document().expect("Document not found");

    let overlay = document.create_element("div").unwrap();
    overlay.set_id("rfd-overlay");

    let card = {
        let card = document.create_element("div").unwrap();
        card.set_id("rfd-card");
        overlay.append_child(&card).unwrap();

        card
    };

    let _link = {
        let link_el = document.create_element("a").unwrap();
        let link: HtmlAnchorElement = wasm_bindgen::JsCast::dyn_into(link_el).unwrap();

        link.set_download(name_hint);
        link.set_href(&url);
        link.set_inner_text("click to download file");

        card.append_child(&link).unwrap();
        link
    };

    let button = {
        let btn_el = document.create_element("button").unwrap();
        let btn: HtmlButtonElement = wasm_bindgen::JsCast::dyn_into(btn_el).unwrap();

        btn.set_id("rfd-button");
        btn.set_inner_text("Ok");

        card.append_child(&btn).unwrap();
        btn
    };

    let style = document.create_element("style").unwrap();
    style.set_inner_html(include_str!("./style.css"));
    overlay.append_child(&style).unwrap();

    let window = web_sys::window().expect("Window not found");
    let document = window.document().expect("Document not found");
    let body = document.body().expect("document should have a body");

    let overlay1 = overlay.clone();
    let url1 = url.clone();

    let closure = Closure::wrap(Box::new(move || {
        println!("downloaded {}", url1.to_string());
        overlay1.remove();
    }) as Box<dyn FnMut()>);

    button.set_onclick(Some(closure.as_ref().unchecked_ref()));
    closure.forget();
    body.append_child(&overlay).ok();
    Ok(())
}
